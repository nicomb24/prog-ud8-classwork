package com.mengual.prog.A3;

public class Moto extends Vehiculo {

    private int cilindrada;
    private int potencia;

    public Moto(String identificador, String color, String modelo, int cilindrada, int potencia) {

        super(identificador, color, modelo);
        this.cilindrada = cilindrada;
        this.potencia = potencia;

    }

    @Override
    public String toString() {

        return super.toString() +
                "Cilindrada: " + cilindrada + ", " +
                "Potencia: " + potencia + " CV\n";

    }
}
