package com.mengual.prog.A3;

public class Camion extends Vehiculo{

    private int cilindrada;
    private int longitud;

    public Camion(String identificador, String color, String modelo, int cilindrada, int longitud) {

        super(identificador, color, modelo);
        this.cilindrada = cilindrada;
        this.longitud = longitud;

    }

    @Override
    public String toString() {

        return super.toString() +
                "Cilindrada: " + cilindrada + ", " +
                "Longitud: " + longitud + " m\n";

    }
}
