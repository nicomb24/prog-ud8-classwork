package com.mengual.prog.A3;

public class Coche extends Vehiculo{

    private int cilindrada;
    private int numeroPuertas;

    public Coche(String identificador, String color, String modelo, int cilindrada, int numeroPuertas) {

        super(identificador, color, modelo);
        this.cilindrada = cilindrada;
        this.numeroPuertas = numeroPuertas;
    }

    @Override
    public String toString() {

        return super.toString() +
                "Cilindrada: " + cilindrada + ", " +
                "Número de puertas: " + numeroPuertas + "\n";

    }
}
