package com.mengual.prog.A3;

import java.util.ArrayList;

public class Taller {

    private ArrayList<Vehiculo> llistatEnReparacio;
    private ArrayList<Vehiculo> llistatReparats;

    public Taller() {

        this.llistatEnReparacio = new ArrayList<>();
        this.llistatReparats = new ArrayList<>();

    }

    public void registrarVehiculo(Vehiculo vehiculo){

        this.llistatEnReparacio.add(vehiculo);

    }

    private Vehiculo getVehiculoReparado(String identificador){

        for (int i = 0; i < llistatReparats.size(); i++ ){

            Vehiculo vehiculo = llistatReparats.get(i);

            if (vehiculo.getIdentificador().equalsIgnoreCase(identificador)){
                return vehiculo;
            }

        }

        return null;

    }

    private Vehiculo getVehiculoEnRaparacion(String identificador){

        for (int i = 0; i < llistatEnReparacio.size(); i++ ){

            Vehiculo vehiculo = llistatEnReparacio.get(i);

            if (vehiculo.getIdentificador().equalsIgnoreCase(identificador)){
                return vehiculo;
            }

        }

        return null;

    }

    public void registrarReparado(String identificador){

        Vehiculo vehiculo = this.getVehiculoEnRaparacion(identificador);

        if (vehiculo != null){

            this.llistatEnReparacio.remove(vehiculo);
            this.llistatReparats.add(vehiculo);

        }

    }

    public void registrarSalida(String identificador){

        Vehiculo vehiculo = this.getVehiculoReparado(identificador);

        if (vehiculo != null){

            this.llistatReparats.remove(vehiculo);

        }

    }

    public void listarEnReparacion(){

        for (int i = 0; i < llistatEnReparacio.size(); i++){

            Vehiculo vehiculo = this.llistatEnReparacio.get(i);

            System.out.println(vehiculo);

        }

    }

    public void listarReparados(){

        for (int i = 0; i < llistatReparats.size(); i++){

            Vehiculo vehiculo = this.llistatReparats.get(i);

            System.out.println(vehiculo);

        }

    }
}
