package com.mengual.prog.A3;

abstract public class Vehiculo {

    protected String identificador;
    protected String color;
    protected String modelo;

    public Vehiculo(String identificador, String color, String modelo) {

        this.identificador = identificador;
        this.color = color;
        this.modelo = modelo;

    }

    public String getIdentificador(){

        return this.identificador;

    }

    @Override
    public String toString() {

        return "Identificador: " + identificador + ", " +
                "Color: " + color + ", " +
                "Modelo: " + modelo + ", ";

    }
}
