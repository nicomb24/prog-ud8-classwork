package com.mengual.prog.A3;

import java.util.Scanner;

public class Menu {

    private Taller taller;

    private Scanner scanner;

    private final int OP_REGISTRAR = 1;
    private final int OP_LISTA_EN_REPARACION = 2;
    private final int OP_LISTA_REPARADOS = 3;
    private final int OP_REGISTRAR_REPARADO = 4;
    private final int OP_REGISTRAR_SALIDA = 5;
    private final int OP_EXIT = 10;
    private final int OP_REGISTRAR_COCHE = 1;
    private final int OP_REGISTRAR_MOTO = 2;
    private final int OP_REGISTRAR_CAMION = 3;
    private final int OP_REGISTRAR_BICICLETA = 4;

    public Menu() {

        this.taller = new Taller();
        this.scanner = new Scanner(System.in);

    }

    private int eleccionMenu(){

        System.out.println(OP_REGISTRAR + ". Registrar vehiculo");
        System.out.println(OP_LISTA_EN_REPARACION + ". Listado de vehiculos en reparación");
        System.out.println(OP_LISTA_REPARADOS + ". Listado de vehiculos reparados");
        System.out.println(OP_REGISTRAR_REPARADO + ". Registrar vehiculo reparado");
        System.out.println(OP_REGISTRAR_SALIDA + ". Registrar salida de vehiculo");
        System.out.println(OP_EXIT + ". Salir");

        return scanner.nextInt();

    }

    public void menu(){

        int opcion;

        do {

            opcion = eleccionMenu();

            if (opcion == OP_REGISTRAR) {
                registrarVehiculo();
            } else if (opcion == OP_LISTA_EN_REPARACION) {
                listarEnReparacion();
            } else if (opcion == OP_LISTA_REPARADOS) {
                listarReparados();
            } else if (opcion == OP_REGISTRAR_REPARADO) {
                registrarReparado();
            } else if (opcion == OP_REGISTRAR_SALIDA) {
                registrarSalida();
            }

        } while (opcion != OP_EXIT);

    }

    private void registrarVehiculo(){

        int opcion = eleccionVehiculo();

        if (opcion == OP_REGISTRAR_COCHE){

            Vehiculo newCoche = new Coche(identificadorVehiculo(),
                    colorVehiculo(), modeloVehiculo(),
                    cilindradaVehiculo(), puertasVehiculo());

            taller.registrarVehiculo(newCoche);

        } else if (opcion == OP_REGISTRAR_MOTO){

            Vehiculo newMoto = new Moto(identificadorVehiculo(),
                    colorVehiculo(), modeloVehiculo(),
                    cilindradaVehiculo(), potenciaVehiculo());

            taller.registrarVehiculo(newMoto);

        } else if (opcion == OP_REGISTRAR_CAMION){

            Vehiculo newCamion = new Camion(identificadorVehiculo(),
                    colorVehiculo(), modeloVehiculo(),
                    cilindradaVehiculo(), longitudVehiculo());

            taller.registrarVehiculo(newCamion);

        }

    }

    private int eleccionVehiculo(){

        System.out.println(OP_REGISTRAR_COCHE + ". Registrar coche");
        System.out.println(OP_REGISTRAR_MOTO + ". Registrar moto");
        System.out.println(OP_REGISTRAR_CAMION + ". Registrar camión");
        System.out.println(OP_REGISTRAR_BICICLETA + ". Registrar bicicleta");

        return scanner.nextInt();

    }

    private String identificadorVehiculo(){

        System.out.println("Introduce el identificador");
        return scanner.next();

    }

    private String colorVehiculo(){

        System.out.println("Introduce el color");
        return scanner.next();

    }

    private String modeloVehiculo(){

        System.out.println("Introduce el modelo");
        return scanner.next();

    }

    private int cilindradaVehiculo(){

        System.out.println("Introduce la cilindrada");
        return scanner.nextInt();

    }

    private int puertasVehiculo(){

        System.out.println("Introduce las puertas");
        return scanner.nextInt();

    }

    private int potenciaVehiculo(){

        System.out.println("Introduce la potencia");
        return scanner.nextInt();

    }

    private int longitudVehiculo(){

        System.out.println("Introduce la longitud");
        return scanner.nextInt();

    }

    private void registrarReparado(){

        taller.listarEnReparacion();

        System.out.println("Introduce el identificador del vehiculo que quiere marcar como reparado");

        taller.registrarReparado(scanner.next());

    }

    private void registrarSalida(){

        taller.listarReparados();

        System.out.println("Introduce el identificador del vehiculo que quiere registrar como salida");

        taller.registrarSalida(scanner.next());

    }

    private void listarEnReparacion(){

        taller.listarEnReparacion();

    }

    private void listarReparados(){

        taller.listarReparados();

    }


}
