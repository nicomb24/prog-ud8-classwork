package com.mengual.prog.A2;

import java.util.ArrayList;

public class LlistaPelicules {

    private ArrayList<Pelicula> llistaPelicules;

    public LlistaPelicules() {

        this.llistaPelicules = new ArrayList<>();

    }

    public void addPelicula(Pelicula pelicula){

        llistaPelicules.add(pelicula);

    }

    public void showPelicules(){

        for (int i = 0; i < llistaPelicules.size(); i++){

            System.out.print(llistaPelicules.get(i));

        }

    }

}
