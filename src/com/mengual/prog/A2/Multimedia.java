package com.mengual.prog.A2;

public class Multimedia {

    protected String titol;

    protected String autor;

    protected Format format;

    protected String durada;

    public Multimedia(String titol, String autor, Format format, String durada) {

        this.titol = titol;
        this.autor = autor;
        this.format = format;
        this.durada = durada;

    }

    @Override
    public String toString() {
        return "Titol: " + titol + "\n" +
                "Autor: " + autor + "\n" +
                "Format: " + format + "\n" +
                "Durada: " + durada;
     }
}
