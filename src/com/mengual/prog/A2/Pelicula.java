package com.mengual.prog.A2;

public class Pelicula extends Multimedia {

    private String actorPrincipal;

    private String actriuPrincipal;

    public Pelicula(String titol, String autor, Format format, String durada) {

        super(titol, autor, format, durada);

    }

    public void setActorPrincipal(String actorPrincipal) {

        this.actorPrincipal = actorPrincipal;

    }

    public void setActriuPrincipal(String actriuPrincipal) {

        this.actriuPrincipal = actriuPrincipal;

    }

    @Override
    public String toString() {
        return super.toString() + "\n" +
                "Actor: " + actorPrincipal + "\n" +
                "Actriu: " + actriuPrincipal + "\n";
    }
}
