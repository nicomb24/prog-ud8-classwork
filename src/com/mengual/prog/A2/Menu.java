package com.mengual.prog.A2;

import java.util.Scanner;

public class Menu {

    private LlistaPelicules llistaPelicules;

    private Scanner scanner;

    private final int OP_ADD_PELICULA = 1;
    private final int OP_BUY_PELICULA = 2;
    private final int OP_EXIT = 10;


    public Menu() {

        this.scanner = new Scanner(System.in);
        this.llistaPelicules = new LlistaPelicules();

    }

    private int eleccionMenu(){

        System.out.println(OP_ADD_PELICULA + ". Añadir pelicula");
        System.out.println(OP_BUY_PELICULA + ". Comprar pelicula");
        System.out.println(OP_EXIT + ". Salir");

        return scanner.nextInt();

    }

    public void menuPrincipal(){

        int eleccion;

        do {

            eleccion = eleccionMenu();

            if (eleccion == OP_ADD_PELICULA) {

                addPelicula();

            } else if (eleccion == OP_BUY_PELICULA) {

                llistaPelicules.showPelicules();

            }

        } while (eleccion != OP_EXIT);

    }

    private void addPelicula(){

        System.out.print("Introduce el título\n");
        String titulo = scanner.next();

        System.out.print("Introduce el autor\n");
        String autor = scanner.next();

        System.out.print("Introduce el formato (WAV, MP3, MIDI, AVI, MOV, MPG, CDAUDIO, DVD)\n");
        String formato = scanner.next();

        System.out.print("Introduce la duración\n");
        String duracion = scanner.next();

        Pelicula newPelicula = new Pelicula(titulo, autor, formatoPelicula(formato), duracion);

        insertActores(newPelicula);

        llistaPelicules.addPelicula(newPelicula);

    }

    private Format formatoPelicula(String formato){

        if (formato.equalsIgnoreCase("mp3")){

            return Format.MP3;

        } else if (formato.equalsIgnoreCase("avi")){

            return Format.AVI;

        } else if (formato.equalsIgnoreCase("cdaudio")){

            return Format.CDAUDIO;

        } else if (formato.equalsIgnoreCase("mpg")){

            return Format.MPG;

        } else if (formato.equalsIgnoreCase("mov")){

            return Format.MOV;

        } else if (formato.equalsIgnoreCase("dvd")){

            return Format.DVD;

        } else if (formato.equalsIgnoreCase("midi")){

            return Format.MIDI;

        } else if (formato.equalsIgnoreCase("wav")){

            return Format.WAV;

        }

        return null;

    }

    private void insertActores(Pelicula pelicula){

        String actor;
        String actriz;


            System.out.println("Introduce la actriz principal");
            actriz = scanner.next();

            System.out.println("Introduce el actor principal");
            actor = scanner.next();


        pelicula.setActorPrincipal(actor);

        pelicula.setActriuPrincipal(actriz);

    }
}
